package com.company;

public abstract class Birds {
    private String food;
    private String area;
    private boolean canFly;
    private boolean canSwim;

    public Birds(String food, String area, boolean canFly, boolean canSwim) {
        this.food = food;
        this.area = area;
        this.canFly = canFly;
        this.canSwim = canSwim;
    }

    public void fly() {
        System.out.println(this.getClass().getSimpleName() + " flying in " + this.area);
    }

    public void run() {
        System.out.println(this.getClass().getSimpleName() + " running in " + this.area);
    }

    public void swim() {
        System.out.println(this.getClass().getSimpleName() + " swimming in " + this.area);
    }

    public void eat() {
        System.out.println(this.getClass().getSimpleName() + " eating the " + this.food);
    }

    public void cry() {
        System.out.println(this.getClass().getSimpleName() + " crying in " + this.area);
    }

    public boolean isCanFly() {
        return canFly;
    }
    public boolean isCanSwim(){
        return canSwim;
    }

}
