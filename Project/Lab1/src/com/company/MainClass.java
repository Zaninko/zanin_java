package com.company;
import java.util.Scanner;

public class MainClass {

    private static final Scanner sc = new Scanner(System.in);
    private static final Activity activity = new Activity();

    public static void main(String[] args) {

        while(true){
          printMenu();
          int comand = sc.nextInt();
           if(comand == 1){
               activity.processFlying("Duck");
               activity.processEating("Duck");
               activity.processRunning("Duck");
               activity.processSwimming("Duck");
               activity.processCrying("Duck");
           }else if(comand == 2){
               activity.processFlying("Penguin");
               activity.processEating("Penguin");
               activity.processRunning("Penguin");
               activity.processSwimming("Penguin");
               activity.processCrying("Penguin");
           }else if(comand == 3){
               activity.processFlying("Kiwi");
               activity.processEating("Kiwi");
               activity.processRunning("Kiwi");
               activity.processSwimming("Kiwi");
               activity.processCrying("Kiwi");
           }else if(comand == 4){
               activity.processFlying("Eagle");
               activity.processEating("Eagle");
               activity.processRunning("Eagle");
               activity.processSwimming("Eagle");
               activity.processCrying("Eagle");
           }else if(comand == 5){
               activity.processFlying("Swallow");
               activity.processEating("Swallow");
               activity.processRunning("Swallow");
               activity.processSwimming("Swallow");
               activity.processCrying("Swallow");
           }else if(comand == 6){
               activity.processFlying("Ostrich");
               activity.processEating("Ostrich");
               activity.processRunning("Ostrich");
               activity.processSwimming("Ostrich");
               activity.processCrying("Ostrich");
           } else
               System.exit(0);
        }
    }
    public static void printMenu(){
        System.out.println("Select an option: ");
        System.out.println("1 - Duck");
        System.out.println("2 - Penguin");
        System.out.println("3 - Kiwi");
        System.out.println("4 - Eagle");
        System.out.println("5 - Swallow");
        System.out.println("6 - Ostrich");
        System.out.println("7 - exit");
    }
}
