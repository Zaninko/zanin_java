package com.company;

import com.company.Birds.*;
import java.util.ArrayList;
import java.util.Arrays;

    public class Activity {

        ArrayList<Birds> birdsList = new ArrayList<>(Arrays.asList(
                new Duck("Fish", "Field"),
                new Eagle("Mouse", "Mountains"),
                new Kiwi("Berrie", "Savanna"),
                new Ostrich("Bugs", "Dessert"),
                new Penguin("Fish", "Arctic"),
                new Swallow("Insects", "Town")
        ));

        public void processFlying(String bird) {
            for (int i = 0; i < birdsList.size(); i++) {
                if (birdsList.get(i).getClass().getSimpleName().equalsIgnoreCase(bird)) {
                    if (birdsList.get(i).isCanFly()) {
                        birdsList.get(i).fly();
                    } else {
                        System.out.println("This bird can`t fly.");
                    }
                }
            }
        }
        public void processEating(String bird) {
            for (int i = 0; i < birdsList.size(); i++) {
                if (birdsList.get(i).getClass().getSimpleName().equalsIgnoreCase(bird)) {
                    birdsList.get(i).eat();
                }
            }
        }
        public void processRunning(String bird) {
            for (int i = 0; i < birdsList.size(); i++) {
                if (birdsList.get(i).getClass().getSimpleName().equalsIgnoreCase(bird)) {
                    birdsList.get(i).run();
                }
            }
        }
        public void processSwimming(String bird) {
                for (int i = 0; i < birdsList.size(); i++) {
                    if (birdsList.get(i).getClass().getSimpleName().equalsIgnoreCase(bird)) {
                        if(birdsList.get(i).isCanSwim()){
                            birdsList.get(i).swim();
                        }else{
                            System.out.println("This bird can't swim");
                        }
                    }
                }
            }
        public void processCrying(String bird) {
            for (int i = 0; i < birdsList.size(); i++) {
                if (birdsList.get(i).getClass().getSimpleName().equalsIgnoreCase(bird)) {
                    birdsList.get(i).cry();
                }
            }
        }
}
