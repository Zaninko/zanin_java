import java.util.List;

public class Activity {
    private final static String name = "Entertainment";
    private List<ContractEmployee> contractEmployees;
    private List<SalariedEmployee> salariedEmployees;
    public String getPlayMEntName(){
        return this.name;
    }
    public Activity(List<ContractEmployee> contractEmployees, List<SalariedEmployee> salariedEmployees){
        this.contractEmployees = contractEmployees;
        this.salariedEmployees = salariedEmployees;
    }
    public void info(){
        for(ContractEmployee contractEmployee: contractEmployees){
            System.out.println("( " + contractEmployee.employeeID + ", Age: " + contractEmployee.age + ", "
                    + contractEmployee.education + ") " + contractEmployee.lastName + ", " +
                    contractEmployee.firstName +" is employee of " + this.getPlayMEntName() + ", federalTaxIDmember: " +
                    contractEmployee.federalTaxIDmember);
        }
        for(SalariedEmployee salariedEmployee: salariedEmployees){
            System.out.println("( " + salariedEmployee.employeeID + ", Age: " + salariedEmployee.age + ", "
                    + salariedEmployee.education + ") " + salariedEmployee.lastName + ", " +
                    salariedEmployee.firstName +" is employee of " + this.getPlayMEntName() + ", socialSecurityNumber: " +
                    salariedEmployee.socialSecurityNumber);
        }
    }

    public double getPayments(Employee e){
        double pay = e.calculatePay();

        if(e.age <= 18){
            final double percent = 0.3;
            pay *= percent;
        }else if(e.age > 18 && e.age <= 40){
            final double percent = 1.1;
            pay *= percent;
        }else if(e.age > 40 && e.age <= 65){
            final double percent = 0.7;
            pay *= percent;
        }else if(e.age > 65){
            final double percent = 0.5;
            pay *= percent;
        }else{
            System.out.println("input is wrong");
        }
        System.out.println(e.lastName + " " + e.firstName + " got Salary $: ");

        return (double)Math.round(pay*100)/100;

    }

}
